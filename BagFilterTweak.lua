-- Replace the original filter-checking method with one that allows more mixes of filtering
function EA_Window_Backpack.CheckCustomFiltersForMatch(itemData, tableOfFilters)
	
	local MainMenuIndex, SubMenuIndex
	local filterFunction, filterData
	local filterGroupsEvaluated = {}
	
	for filterID in pairs( tableOfFilters ) do
	
		MainMenuIndex, SubMenuIndex = EA_Window_Backpack.GetFilterIndicesFromID( filterID )
	
		filterData = EA_Window_Backpack_Filters.menus[MainMenuIndex][SubMenuIndex]
		filterFunction = filterData.filter
		if filterFunction == nil or type(filterFunction) ~= "function" then
			ERROR(L"EA_Window_Backpack.CheckCustomFiltersForMatch filter found with no test function. filterID="..filterID)
        else
		
            -- check filter
            -- though can skip check if one of the filters from the same filterGroup already matched
            if not filterGroupsEvaluated[filterData.filterGroup] then
                filterGroupsEvaluated[filterData.filterGroup] = filterFunction( itemData ) 
            end
        end
	end
	
    local somethingMatched = false
    
	-- check each filter group marked to make sure it had at least one filter match
	for group, passed in pairs( filterGroupsEvaluated ) do
		if passed then
            somethingMatched = true
        else
            -- is this the usability/rarity filter? if so, bomb out early (6 = usability, 7 = rarity)
            if (group == 6) or (group == 7) then
                return false
            end
		end
	end
    
    return somethingMatched
end