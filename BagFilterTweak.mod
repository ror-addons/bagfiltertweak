<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="BagFilterTweak" version="1.1" date="27/10/2008" >

		<Author name="Aiiane" email="aiiane@aiiane.net" />
		<Description text="Tweaks the backpack filtering to be a little more useful." />
		
        <Dependencies>
            <Dependency name="EA_BackpackWindow" />
        </Dependencies>
        
		<Files>
            <File name="BagFilterTweak.lua" />
		</Files>
		
	</UiMod>
</ModuleFile>
